## AWS DOC
[S3](https://docs.aws.amazon.com/zh_tw/sdk-for-java/v1/developer-guide/examples-s3-transfermanager.html)  
[java_sdk](https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/examples-s3.html)

## AWS Sample code
[code]( https://github.com/awsdocs/aws-doc-sdk-examples/tree/master/java)