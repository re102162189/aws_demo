package com.tgl.aws.test;

import java.io.File;
import java.time.Duration;
import java.time.Instant;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.transfer.Download;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;

public class UploadObject {

  private final static String ACCESS_KEY_ID = "";

  private final static String SECRET_KEY_ID = "";

  private final static Regions REGIONS = Regions.AP_SOUTHEAST_2;

  private final static String UPLOAD_FILE_PATH = "";
  
  private final static String DOWNLOAD_FILE_PATH = "";

  private final static String BUCKET_NAME = "myfirsttglbucket";
  
  private final static String KEY_NAME = "myfirsttglfilem";

  public static void main(String[] args) {
    
    // preparing upload file
    ClassLoader classLoader = UploadObject.class.getClassLoader();
    File uploadFile = new File(classLoader.getResource(UPLOAD_FILE_PATH).getFile());
    File downloadFile = new File(DOWNLOAD_FILE_PATH);

    // init s3 client with region & credentials settings
    BasicAWSCredentials awsCreds = new BasicAWSCredentials(ACCESS_KEY_ID, SECRET_KEY_ID);
    AWSStaticCredentialsProvider awsCredsProvider =new AWSStaticCredentialsProvider(awsCreds);
    AmazonS3 s3Client = AmazonS3ClientBuilder
        .standard()
        .withCredentials(awsCredsProvider)
        .withRegion(REGIONS)
        .build();

    Bucket bucket = s3Client.createBucket(BUCKET_NAME);
    System.out.println(String.format("Bucket created: %s in Region: %s", bucket.getName(), REGIONS));
   
    // up/download using transfer manager 
    TransferManager xferMgr = TransferManagerBuilder.standard().withS3Client(s3Client).build();
    
    Instant start = Instant.now();
    try {
      Upload upload = xferMgr.upload(BUCKET_NAME, KEY_NAME, uploadFile);
      XferMgrProgress.showTransferProgress(upload);
      XferMgrProgress.waitForCompletion(upload);
    } catch (AmazonServiceException e) {
      System.err.println(e.getErrorMessage());
      System.exit(1);
    }
    Duration duration = Duration.between(start, Instant.now());
    long uploadTime = duration.getSeconds();
    
    start = Instant.now();
    try {
      Download download = xferMgr.download(BUCKET_NAME, KEY_NAME, downloadFile);
      XferMgrProgress.showTransferProgress(download);
      XferMgrProgress.waitForCompletion(download);
    } catch (AmazonServiceException e) {
      System.err.println(e.getErrorMessage());
      System.exit(1);
    }
    duration = Duration.between(start, Instant.now());
    long downloadTime = duration.getSeconds();
    
    System.out.println(String.format("TransferManager upload total elapsed: %s sec(s)", uploadTime));
    System.out.println(String.format("TransferManager download total elapsed: %s sec(s)", downloadTime));
    
    // remove elements from test bucket
    s3Client.deleteObject(BUCKET_NAME, KEY_NAME);
    s3Client.deleteBucket(BUCKET_NAME);
    System.out.println(String.format("Bucket deleted: %s in Region: %s", bucket.getName(), REGIONS));
    
    xferMgr.shutdownNow();
  }
}
