package com.tgl.aws.test;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.transfer.Transfer;
import com.amazonaws.services.s3.transfer.TransferProgress;

public final class XferMgrProgress {

  private static final int BAR_SIZE = 100;

  private static final String EMPTY_BAR;

  private static final String FILLED_BAR;

  static {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < BAR_SIZE; i++) {
      sb.append(" ");
    }
    EMPTY_BAR = sb.toString();

    sb = new StringBuilder();
    for (int i = 0; i < BAR_SIZE; i++) {
      sb.append("#");
    }
    FILLED_BAR = sb.toString();
  }

  public static void waitForCompletion(Transfer xfer) {
    try {
      xfer.waitForCompletion();
    } catch (AmazonServiceException e) {
      System.err.println("Amazon service error: " + e.getMessage());
      System.exit(1);
    } catch (AmazonClientException e) {
      System.err.println("Amazon client error: " + e.getMessage());
      System.exit(1);
    } catch (InterruptedException e) {
      System.err.println("Transfer interrupted: " + e.getMessage());
      System.exit(1);
    }
  }

  public static void showTransferProgress(Transfer xfer) {
    System.out.println(xfer.getDescription());
    int pctState = 0;
    
    do {
      TransferProgress progress = xfer.getProgress();
      double pct = progress.getPercentTransferred();
      if ((int) pct % 5 == 0 && (int) pct != pctState) {
        printProgressBar(pct);
        pctState = (int) pct;
      }
    } while (!xfer.isDone());
  }

  private static void printProgressBar(double pct) {
    int amtFull = (int) (BAR_SIZE * (pct / 100.0));
    System.out.format("[%s%s] ~~ %s percent \n", FILLED_BAR.substring(0, amtFull),
        EMPTY_BAR.substring(0, BAR_SIZE - amtFull), String.format("%.2f", pct));
  }
}
